<?php 

session_start();

include('includes/php/update.inc.php');

$id 	= isset($_GET['id']) ? $_GET['id'] : 1;
$sql 	= 'SELECT * FROM articles, categories WHERE articles.category_id=categories.id AND articles.id ='.$id;
$stmt = $dbc->prepare($sql);
$OK 	= $stmt->execute();
$row 	= $stmt->fetch();
if(!$row) {
	$_SESSION['error'] = 'Error: Article does not exist.';
	header('Location: articles.php');
}

$stmt = $dbc->prepare('SELECT * FROM categories');
$OK 	= $stmt->execute();
$dbc 	= null;

$currentPage = basename($_SERVER['SCRIPT_NAME']); 

?>

<?php include('includes/html/head.inc.php'); ?>

	<body>
		
<!-- CONTAINER --------------------------->	
		<div class="container-fluid">
			
			<?php include('includes/html/header.inc.php'); ?>				
			
<!---- MAIN ROW ------------------------------------------>
			<div class="main-row row">
				<div class="col-xs-12 col-sm-12 col-md-12">

					<div class="row">
						<div class="row-title main-row-title text-center col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
							<div><h3>Update</h3></div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-4 col-lg-offset-4">
							
							<form method="POST" onsubmit="return validate()" >
								
								<div class="form-group">
									<label for="category_id">Category</label>
									<select class="form-control" id="category_id" name="category_id">
										<?php while($categories = $stmt->fetch()) { ?>
											<option value="<?php echo $categories['id']; ?>"><?php echo $categories['category']; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group hidden"><!-- HIDDEN UNTIL FUNCTIONALITY CAN BE ADDED -->
									<label for="image">Replace the Image (Optional)</label>
									<input type="file" id="image" name="image" />
									<p class="help-block">Images must be smaller than 2MB in size.</p>
								</div>
								<div class="form-group">
									<label for="error_text">Error Text</label>
									<input type="text" class="form-control" id="error_text" name="error_text" value="<?php echo(isset($row['error_text']) ? htmlspecialchars($row['error_text']) : ''); ?>" />
								</div>
								<div class="form-group">
									<label for="analysis">Analysis</label>
									<textarea type="text" class="form-control" id="analysis" name="analysis" rows=4><?php echo(isset($row['analysis']) ? htmlspecialchars($row['analysis']) : ''); ?></textarea>
								</div>
								<div class="form-group">
									<label for="context">Context</label>
									<input type="text" class="form-control" id="context" name="context" value="<?php echo(isset($row['context']) ? htmlspecialchars($row['context']) : ''); ?>" />
								</div>
								<input type="hidden" name="id" value="<?php echo(isset($row['id']) ? htmlspecialchars($row['id']) : ''); ?>" />
								<div class="row">
									<button type="submit" name="submit" value="update" class="btn btn-primary col-xs-5 col-xs-offset-1 col-sm-6 col-sm-offset-1 col-md-6 col-md-offset-1">Update</button>
									<button type="submit" name="submit" value="cancel" class="btn btn-danger col-xs-4 col-xs-offset-1 col-sm-3 col-sm-offset-1 col-md-3 col-md-offset-1">Cancel</button>
								</div>
								
							</form>	
							
						</div>
					</div>
					
				</div>
			</div>
<!---- MAIN ROW END -------------------------------------->
			
			
			<?php include('includes/html/footer.inc.php'); ?>
			
		</div>
<!-- CONTAINER END ----------------------->
		
		<?php include('includes/html/resources.inc.php'); ?>
		
		<script async src="js/validate_form.js"></script>
		
	</body>
</html>