<?php 

session_start();

include('includes/php/upload.inc.php');


$currentPage = basename($_SERVER['SCRIPT_NAME']); 

?>

<?php include('includes/html/head.inc.php'); ?>

	<body>
		
<!-- CONTAINER --------------------------->	
		<div class="container-fluid">

			<?php include('includes/html/header.inc.php'); ?>

<!---- MAIN ROW ------------------------------------------>
			<div class="main-row row">
				<div class="col-xs-12 col-sm-12 col-md-12">

					<div class="row">
						<div class="row-title main-row-title text-center col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
							<div><h3>Home</h3></div>
						</div>
					</div>
					
					<?php include('includes/php/notifications.inc.php'); ?>
					<?php if(isset($_SESSION['success'])) { unset($_SESSION['success']); } ?>
					<?php if(isset($_SESSION['error'])) { unset($_SESSION['error']); } ?>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
							
							<div class="hidden-xs hidden-sm">
								<div class="page-header" style="margin: 0;">
									<h1>Assessment 1 <small>Individual Blog Website</small></h1>
									<hr style="border: solid 1px grey;" />
								</div>
								<p class="lead pull-left"><strong>STUDENT NAME:</strong> Jeremy Gates</p>
								<p class="lead pull-right"><strong>STUDENT NUMBER:</strong> s3138222</p>
							</div>
							
							<div class="hidden-md hidden-lg">
								<div class="page-header" style="margin: 0;">
									<h1>Assessment 1</h1>
									<h1><small>Individual Blog Website</small></h1>
									<hr style="border: solid 1px grey;" />
								</div>
								<p class="lead"><strong>STUDENT NAME:</strong> Jeremy Gates</p>
								<p class="lead"><strong>STUDENT NUMBER:</strong> s3138222</p>
							</div>
							
						</div>
					</div>
					
					
				</div>
			</div>
<!---- MAIN ROW END -------------------------------------->


			<?php include('includes/html/footer.inc.php'); ?>

		</div>
<!-- CONTAINER END ----------------------->
		
		<?php include('includes/html/resources.inc.php'); ?>
		
		
	</body>
		<script type="text/javascript">
			//Set size of background image to window size
			$(".main-row").css("min-height", $(window).height() - 318);
		</script>
</html>