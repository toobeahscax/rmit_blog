function validate() {
	
	//DISTINGUISH CREATE VS EDIT PAGE
	var page_title	= $(document).attr('title');
	
	var type 				= document.create.type.value;
	var image 			= document.create.image.value;
	var error_text 	= document.create.error_text.value;
	var analysis 		= document.create.analysis.value;
	var context 		= document.create.context.value;
	var valid 			= true;

	if(type == "") {
		$("#warn-type").html('Error type is required.');
		valid = false;
	} else { $("#warn-type").html(''); }
	//IMAGE ONLY REQUIRED WHEN CREATING AN ARTICLE
	if(image == "" && page_title == 'Bloggates | Create') {
		$("#warn-image").html('You must upload an image.');
		valid = false;
	} else { $("#warn-image").html(''); }
	if(error_text == "") {
		$("#warn-error_text").html('Error text is required.');
		valid = false;
	} else { $("#warn-error_text").html(''); }
	if(analysis == "") {
		$("#warn-analysis").html('Please analyse why this error occured.');
		valid = false;
	} else { $("#warn-analysis").html(''); }
	if(context == "") {
		$("#warn-context").html('Error context is required.');
		valid = false;
	} else { $("#warn-context").html(''); }
	if(!valid) {
		return false;
	}
}