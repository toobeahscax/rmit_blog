<?php

require_once 'connection.inc.php';

if(array_key_exists('category_id', $_POST)) {
	
	//DEFINE THE UPLOAD DIRECTORY
	define('UPLOAD_DIR', '/Users/Sandman/Dropbox/web-content/rmit/blog/images/'); //MAC
	//define('UPLOAD_DIR', 'C:/Users/Jerem/Dropbox/web-content/rmit/blog/images/');		//PC
	//define('UPLOAD_DIR', 'images/');																							//AZURE
	//SET TIMEZONE TO MELBOURNE
	date_default_timezone_set('Australia/Melbourne');
	
	$category_id 		= trim($_POST['category_id']);
	//RENAME FILE TO REMOVE SPACES AND PREVENT DUPLICATE FILENAME - ONE FOR THUMBNAIL, ONE FOR ARTICLE
	$fname_thumb 		= UPLOAD_DIR.date('Y.m.d.H.i.s-').'200h-'.str_replace(' ', '_', $_FILES['image']['name']);
	$fname_article 	= UPLOAD_DIR.date('Y.m.d.H.i.s-').'690w-'.str_replace(' ', '_', $_FILES['image']['name']);
	//STORE IMAGE NAMES IN DATABASE
	$image_thumb		= date('Y.m.d.H.i.s-').'200h-'.str_replace(' ', '_', $_FILES['image']['name']);
	$image_article	= date('Y.m.d.H.i.s-').'690w-'.str_replace(' ', '_', $_FILES['image']['name']);
	$error_text			= trim($_POST['error_text']);
	$analysis				= trim($_POST['analysis']);
	$context				= trim($_POST['context']);
	$date						= date("Y-m-d H:i:s");

	$sql = 'INSERT INTO articles (category_id, image_thumb, image_article, error_text, analysis, context, created_at, updated_at) VALUES (:category_id, :image_thumb, :image_article, :error_text, :analysis, :context, :created_at, :updated_at)';
	
	//CHECK UPLOADED IMAGE
	if(is_uploaded_file($_FILES['image']['tmp_name'])) {
		copy($_FILES['image']['tmp_name'], $fname_thumb);
		copy($_FILES['image']['tmp_name'], $fname_article);
		//ONLY INSERT RECORD IF IMAGE OK
		$stmt = $dbc->prepare($sql);
		if($stmt) {
			$OK = $stmt->execute(array(':category_id'=>$category_id, ':image_thumb'=>$image_thumb, ':image_article'=>$image_article, ':error_text'=>$error_text, ':analysis'=>$analysis, ':context'=>$context, ':created_at'=>$date, ':updated_at'=>$date));
		}
	} else {
		$errors = 'Could not save file! Upload Error Code: '.$_FILES['image']['error'];
		exit;
	}
	
	if($OK) {
		$_SESSION['success'] = 'Article successfully added.';
		header('Location: article.php?id='.$dbc->lastInsertId());
		$dbc = null;
		exit;
	} else {
		$errors .= '<br/>Could not insert record: '.$stmt->errorInfo();
	}
	$stmt = null;
}

?>