<!-------- NOTIFICATION AREA ----------------------------->
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
							<div class="notifications">
								<?php

								if(isset($errors)) {
									if(is_array($errors)) {
										echo "<div class='alert alert-danger text-center'><pre>".print_r($errors)."</pre></div>";
									} else {
										echo "<div class='alert alert-danger text-center'>$errors</div>";
									}
								}
								
								if(isset($_SESSION['error'])) {
									$error = $_SESSION['error'];
									echo "<div class='alert alert-danger text-center'>$error</div>";
								}

								isset($_SESSION['success']) ? $success = $_SESSION['success'] : $success = null;
								if(isset($success)) {
									echo "<div class='alert alert-success text-center'>$success</div>";
								}

								?>
							</div>
						</div>
					</div>
<!-------- END NOTIFICATION AREA ------------------------->