<?php

require_once 'connection.inc.php';

if(array_key_exists('id', $_POST)) {
	
	$article_to_delete = $_POST['id'];
	
	$sql = 'DELETE FROM articles WHERE id = :id';

	$stmt = $dbc->prepare($sql);																	//Initialise the statement
	if($stmt) {																										//Validate query
		$OK = $stmt->execute(array(':id'=>$article_to_delete));
	}

	$dbc = null;	//Close db connection

	if($OK) {																											//If article inserted correctly
		$_SESSION['success'] = 'Article successfully deleted.';
		header('Location: articles.php');
		exit;
	} else {
		$errors = $stmt->errorInfo();
	}
	$stmt = null;
}