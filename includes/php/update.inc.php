<?php

require_once 'connection.inc.php';

if(isset($_POST['submit']) && $_POST['submit'] == 'cancel') {
	header('Location: article.php?id='.$_POST['id']);
	exit;
} elseif(isset($_POST['submit']) && $_POST['submit'] == 'update') {
	//DEFINE THE UPLOAD DIRECTORY
	define('UPLOAD_DIR', '/Users/Sandman/Dropbox/web-content/rmit/blog/images/');
	//SET TIMEZONE TO MELBOURNE
	date_default_timezone_set('Australia/Melbourne');
	
	$id							= trim($_POST['id']);
	$category_id		= trim($_POST['category_id']);
	//RENAME FILE TO REMOVE SPACES AND PREVENT DUPLICATE FILENAME - ONE FOR THUMBNAIL, ONE FOR ARTICLE
	//$fname_thumb 		= UPLOAD_DIR.date('Y.m.d.H.i.s-').'200h-'.str_replace(' ', '_', $_FILES['image']['name']);
	//$fname_article 	= UPLOAD_DIR.date('Y.m.d.H.i.s-').'690w-'.str_replace(' ', '_', $_FILES['image']['name']);
	//STORE IMAGE NAMES IN DATABASE
	//$image_thumb		= date('Y.m.d.H.i.s-').'200h-'.str_replace(' ', '_', $_FILES['image']['name']);
	//$image_article	= date('Y.m.d.H.i.s-').'690w-'.str_replace(' ', '_', $_FILES['image']['name']);
	$error_text			= trim($_POST['error_text']);
	$analysis				= trim($_POST['analysis']);
	$context				= trim($_POST['context']);
	$date						= date("Y-m-d H:i:s");

	//$sql = 'UPDATE articles SET category_id=:category_id, image_thumb=:image_thumb, image_article=:image_article, error_text=:error_text, analysis=:analysis, context=:context, updated_at=:updated_at WHERE id = :id';
	$sql = 'UPDATE articles SET category_id=:category_id, error_text=:error_text, analysis=:analysis, context=:context, updated_at=:updated_at WHERE id ='.$id;
	
	$stmt = $dbc->prepare($sql);
	if($stmt) {
		//$OK = $stmt->execute(array(':category_id'=>$category_id, ':image_thumb'=>$image_thumb, ':image_article'=>$image_article, ':error_text'=>$error_text, ':analysis'=>$analysis, ':context'=>$context, ':updated_at'=>$date, ':id'=>$id));
		$OK = $stmt->execute(array(':category_id'=>$category_id, ':error_text'=>$error_text, ':analysis'=>$analysis, ':context'=>$context, ':updated_at'=>$date));
	}
	//CHECK UPLOADED IMAGE
//	if(is_uploaded_file($_FILES['image']['tmp_name'])) {
//		copy($_FILES['image']['tmp_name'], $fname_thumb);
//		copy($_FILES['image']['tmp_name'], $fname_article);
//		//ONLY INSERT RECORD IF IMAGE OK
//		$stmt = $dbc->prepare($sql);
//		if($stmt) {
//			$OK = $stmt->execute(array(':type'=>$type, ':image_thumb'=>$image_thumb, ':image_article'=>$image_article, ':error_text'=>$error_text, ':analysis'=>$analysis, ':context'=>$context, ':updated_at'=>$date, ':id'=>$id));
//		}
//	} else {
//		$errors = 'Could not save file! Upload Error Code: '.$_FILES['image']['error'];
//		exit;
//	}
	
	if($OK) {
		$_SESSION['success'] = 'Article successfully updated.';
		header('Location: article.php?id='.$id);
		$dbc = null;
		exit;
	} else {
		$errors .= '<br/>Could not insert record: '.$stmt->errorInfo();
	}
	$stmt = null;
}

?>