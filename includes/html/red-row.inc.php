<!---- RED ROW ------------------------------------------->
			<div class="red-row row">
				<div class="row">
					<div class="row-title red-row-title text-center col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
						<div><h3>Search</h3></div>
					</div>
				</div>

				<form method="post">
					<div class="form-group col-xs-12 col-sm-12 col-md-6 col-md-offset-3">
						<div class="input-group">
							<input class="form-control" type="text" name="search" placeholder="Search articles..." />
							<div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
						</div>
					</div>
				</form>
			</div>
<!---- RED ROW END --------------------------------------->