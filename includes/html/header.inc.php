<!---- HEADER ROW ---------------------------------------->
			<div class="header-row row">
				
				<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-4 col-lg-offset-4">
					<nav class="navbar navbar-default">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsible-navbar" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="collapsible-navbar">
							<ul class="nav navbar-nav">
								<li class="<?php if($currentPage == 'index.php') 		{ echo 'active'; } ?>"><a href="index.php">Home</a></li>
								<li class="<?php if($currentPage == 'articles.php') { echo 'active'; } ?>"><a href="articles.php">Articles</a></li>
								<li class="<?php if($currentPage == 'create.php') 	{ echo 'active'; } ?>"><a href="create.php">Create</a></li>
							</ul>
						</div>
					</nav>
				</div>

				<div class="col-xs-12 col-md-12">
					<div class="tittle text-center">
						<h1>BLOGGATES</h1>
						<p>A Blog About PHP</p>
					</div>
				</div>
					
				
			</div>
<!---- HEADER ROW END ------------------------------------>