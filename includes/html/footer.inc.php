<!---- FOOTER ROW ---------------------------------------->
			<footer class="footer footer-row row">
<!--
				<div class="row">
					<div class="row-title footer-row-title text-center col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
						<div><h3>Footer</h3></div>
					</div>
				</div>
-->	
					<div class="text-center col-xs-12 col-sm-12 col-md-12">
						<ul class="list-inline">
							<li>&copy; <?php echo date('Y'); ?>::Jeremy Gates</li>
							<li>|</li>
							<li>RMIT</li>
							<li class="hidden-xs">|</li>
							<li class="hidden-xs">IT5M</li>
							<li class="hidden-xs">|</li>
							<li class="hidden-xs">Dynamic Websites</li>
						</ul>
					</div>
				
			</footer>
<!---- FOOTER ROW END ------------------------------------>