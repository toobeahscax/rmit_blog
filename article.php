<?php 

session_start();

include('includes/php/delete.inc.php');

$id 	= isset($_GET['id']) ? $_GET['id'] : 0;
$sql 	= 'SELECT * FROM articles, categories WHERE articles.category_id=categories.id AND articles.id ='.$id;
$stmt = $dbc->prepare($sql);
$OK 	= $stmt->execute();
$row 	= $stmt->fetch();
if(!$row) {
	$_SESSION['error'] = 'Error: Article does not exist.';
	header('Location: articles.php');
	exit;
}
$dbc 	= null;

$currentPage = basename($_SERVER['SCRIPT_NAME']); 

?>

<?php include('includes/html/head.inc.php'); ?>

	<body>
		
<!-- CONTAINER --------------------------->	
		<div class="container-fluid">
			
			<?php include('includes/html/header.inc.php'); ?>
						
<!---- MAIN ROW ------------------------------------------>
			<div class="main-row row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">

					<div class="row">
						<div class="row-title main-row-title text-center col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
							<div><h3>Category: <?php echo htmlspecialchars($row['category']); ?></h3></div>
						</div>
					</div>
					
					<?php include('includes/php/notifications.inc.php'); ?>
					<?php if(isset($_SESSION['success'])) { unset($_SESSION['success']); } ?>
					<?php if(isset($_SESSION['error'])) { unset($_SESSION['error']); } ?>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
							
							<article>
								<p class="pull-left"><strong>Author:</strong> Jeremy</p>
								<p class="pull-right"><strong>Updated:</strong> <?php echo htmlspecialchars($row['updated_at']); ?></p>
								<div class="clearfix"></div>
								<div class="text-center">
									<h2>Error Text</h2>
									<p class="lead"><?php echo htmlspecialchars($row['error_text']); ?></p>
								</div>
								<div class="article-img">
									<img width="690" src="images/<?php echo htmlspecialchars($row['image_article']); ?>" class="img-responsive center-block" alt="Responsive Image" />
								</div>
								<p class="lead"><strong>Context: </strong><?php echo htmlspecialchars($row['context']); ?></p>
								<p class="lead"><strong>Analysis: </strong><?php echo htmlspecialchars($row['analysis']); ?></p>
							</article>
							
							<div class="row">
								<div class="col-xs-7 col-sm-6 col-md-5">
									<button class="btn btn-primary btn-block" onclick="window.location.href='edit.php?id=<?php echo htmlspecialchars($row['id']); ?>'">Edit</button>
								</div>
								<form method="POST">
									<input type="hidden" value="<?php echo htmlspecialchars($row['id']); ?>" name="id" />
									<div class="col-xs-5 col-sm-4 col-md-3 pull-right"><button type="submit" class="btn btn-danger btn-block">Delete</button></div>
								</form>
							</div>
							
						</div>
					</div>
					
				</div>
			</div>
<!---- MAIN ROW END -------------------------------------->
			
			
			<?php include('includes/html/footer.inc.php'); ?>
			
		</div>
<!-- CONTAINER END ----------------------->
		
		<?php include('includes/html/resources.inc.php'); ?>
		
	</body>
</html>