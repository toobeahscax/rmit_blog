CREATE DATABASE IF NOT EXISTS s3138222;

USE s3138222;

CREATE TABLE categories(
	id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	category VARCHAR(50)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `categories` (`id`, `category`) VALUES
(1, 'Notice'),
(2, 'Fatal'),
(3, 'Warning'),
(4, 'Parse');

CREATE TABLE articles(
	id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	category_id INT,
	image_thumb VARCHAR(100),
	image_article VARCHAR(100),
	error_text TEXT,
	analysis TEXT, 
	context TEXT, 
	created_at DATETIME DEFAULT NULL, 
	updated_at DATETIME DEFAULT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `articles` (`id`, `category_id`, `image_thumb`, `image_article`, `error_text`, `analysis`, `context`, `created_at`, `updated_at`) VALUES
(1, 1, 'pineapple-day-at-beach-1-200h.jpg', 'pineapple-day-at-beach-1-690w.jpg', 'Undefined variable: dbc in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/php/connection.inc.php on line 10', 'The variable declaration for dbc was contained within a try{} section, but it was never instantiated because the try block failed, leading to this undefined variable error.', 'Trying to setup the connection to the database connection.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(2, 2, 'pineapple-day-at-beach-2-200h.jpg', 'pineapple-day-at-beach-2-690w.jpg', 'Uncaught Error: Call to a member function prepare() on null in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php:11 Stack trace: #0 {main} thrown in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php on line 11', 'Tried to call a function on the previously undefined variable dbc. Can''t call a function on a null instance of the object.', 'Trying to setup the connection to the database connection.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(3, 2, 'pineapple-day-at-beach-3-200h.jpg', 'pineapple-day-at-beach-3-690w.jpg', 'Uncaught PDOException: could not find driver in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php:7 Stack trace: #0 /Users/Sandman/Dropbox/web-content/rmit/blog/test.php(7): PDO->__construct(''dbname=rmit_blo...'', ''user'', ''s3138222'') #1 {main} thrown in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php on line 7', 'I had the DSN string incorrect. I put the database name first, but the host should go first. What surprises me is that the PHP documentation puts the database name first - is this incorrect? I could not get the database connection to work unless I put the host string first.', 'Trying to setup the connection to the database connection.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(4, 3, 'pineapple-day-at-beach-4-200h.jpg', 'pineapple-day-at-beach-4-690w.jpg', 'include(includes/head.inc.php): failed to open stream: No such file or directory in /Users/Sandman/Dropbox/web-content/rmit/blog/index.php on line 2', 'Tried to include a file that didn''t exist. The file does exist, but I had changed the website directory structure and forgotten to update the file location references.', 'In the process of simplifying directory structure.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(5, 3, 'pineapple-day-at-beach-5-200h.jpg', 'pineapple-day-at-beach-5-690w.jpg', 'include(): Failed opening ''data.php'' for inclusion (include_path=''.:/Applications/MAMP/bin/php/php7.0.12/lib/php'') in /Users/Sandman/Dropbox/web-content/rmit/blog/index.php on line 1', 'Tried to include a file that I had previously deleted as it was no longer needed. Neglected to update the page to remove the include statement.', 'In the process of simplifying directory structure.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(6, 2, 'pineapple-day-at-beach-6-200h.jpg', 'pineapple-day-at-beach-6-690w.jpg', 'Call to undefined function fetch() in C:\\Users\\Jerem\\Dropbox\\web-content\\rmit\\blog\\articles.php on line 40', 'This was essentially a syntax error. I had: \$stmt-fetch() instead of: \$stmt->fetch()', 'Trying to loop through all articles and output them to the page.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(7, 3, 'pineapple-day-at-beach-7-200h.jpg', 'pineapple-day-at-beach-7-690w.jpg', 'Cannot modify header information - headers already sent by (output started at /Users/Sandman/Dropbox/web-content/rmit/blog/create.php:3) in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/php/insert.inc.php on line 23', 'I had a header redirect at the end of my insert script, but apparently I have already sent headers (output html), meaning the redirect failed. The database updated correctly, however, so there were no problems with insert script.', 'Trying to insert data into the database via the web form.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(8, 3, 'pineapple-day-at-beach-8-200h.jpg', 'pineapple-day-at-beach-8-690w.jpg', 'Undefined variable: currentPage in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/html/head.inc.php on line 9', 'Needed to add the line \$currentPage = basename(\$_SERVER[''SCRIPT_NAME'']);  to the article page. This variable determines which navbar element to highlight.', 'Going back to edit the article page.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(9, 1, 'pineapple-day-at-beach-9-200h.jpg', 'pineapple-day-at-beach-9-690w.jpg', 'Undefined index: id in /Users/Sandman/Dropbox/web-content/rmit/blog/article.php on line 7', 'Same error as ''Undefined Variable'' but since \$_GET is an array, then it becomes an undefined index ''id'' in the array. When the variable ''id'' is not found in the url, then the index is not instantiated.', 'Trying to set default value for article id if get variable not set.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(10, 4, 'neon-1-200h.jpg', 'neon-1-690w.jpg', 'syntax error, unexpected \'if\' (T_IF) in /Users/Sandman/Dropbox/web-content/rmit/blog/article.php on line 11', 'Inadvertently forgot the line delimiter ;', 'Trying to handle errors when fetching article from the database.', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(11, 1, 'neon-2-200h.jpg', 'neon-2-690w.jpg', 'Array to string conversion in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/php/notifications.inc.php on line 8', '\$stmt->errorInfo() returns an array, but I was trying to echo it out. Must use print_r() instead.', 'Trying to handle errors when fetching article from the database', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(12, 2, 'neon-3-200h.jpg', 'neon-3-690w.jpg', 'Uncaught exception \'PDOException\' with message \'SQLSTATE[21S01]: Insert value list does not match column list: 1136 Column count doesn\'t match value count at row 1\' in C:\\Users\\Jerem\\Dropbox\\web-content\\rmit\\blog\\includes\\php\\insert.inc.php:36 Stack trace: #0 C:\\Users\\Jerem\\Dropbox\\web-content\\rmit\\blog\\includes\\php\\insert.inc.php(36): PDOStatement->execute(Array) #1 C:\\Users\\Jerem\\Dropbox\\web-content\\rmit\\blog\\create.php(5): include(\'C:\\\\Users\\\\Jerem\\\\...\') #2 {main} thrown in C:\\Users\\Jerem\\Dropbox\\web-content\\rmit\\blog\\includes\\php\\insert.inc.php on line 36', 'The amount of attributes I was trying to insert did not match the amount attributes I had declared.', 'Trying to change the insert script to accept two more attributes.', '2017-03-16 10:02:38', '2017-03-16 10:02:38');