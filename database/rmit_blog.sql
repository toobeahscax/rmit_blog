-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 13, 2017 at 11:34 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rmit_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `image_thumb` varchar(100) DEFAULT NULL,
  `image_article` varchar(100) DEFAULT NULL,
  `error_text` text,
  `analysis` text,
  `context` text,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `type`, `image_thumb`, `image_article`, `error_text`, `analysis`, `context`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Notice', 'pineapple-day-at-beach-1-200h.jpg', 'pineapple-day-at-beach-1-690w.jpg', 'Undefined variable: dbc in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/php/connection.inc.php on line 10', 'The variable declaration for dbc was contained within a try{} section, but it was never instantiated because the try block failed, leading to this undefined variable error.', 'Trying to setup the connection to the database connection.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(2, 'Fatal', 'pineapple-day-at-beach-2-200h.jpg', 'pineapple-day-at-beach-2-690w.jpg', 'Uncaught Error: Call to a member function prepare() on null in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php:11 Stack trace: #0 {main} thrown in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php on line 11', 'Tried to call a function on the previously undefined variable dbc. Can''t call a function on a null instance of the object.', 'Trying to setup the connection to the database connection.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(3, 'Fatal', 'pineapple-day-at-beach-3-200h.jpg', 'pineapple-day-at-beach-3-690w.jpg', 'Uncaught PDOException: could not find driver in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php:7 Stack trace: #0 /Users/Sandman/Dropbox/web-content/rmit/blog/test.php(7): PDO->__construct(''dbname=rmit_blo...'', ''user'', ''s3138222'') #1 {main} thrown in /Users/Sandman/Dropbox/web-content/rmit/blog/test.php on line 7', 'I had the DSN string incorrect. I put the database name first, but the host should go first. What surprises me is that the PHP documentation puts the database name first - is this incorrect? I could not get the database connection to work unless I put the host string first.', 'Trying to setup the connection to the database connection.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(4, 'Warning', 'pineapple-day-at-beach-4-200h.jpg', 'pineapple-day-at-beach-4-690w.jpg', 'include(includes/head.inc.php): failed to open stream: No such file or directory in /Users/Sandman/Dropbox/web-content/rmit/blog/index.php on line 2', 'Tried to include a file that didn''t exist. The file does exist, but I had changed the website directory structure and forgotten to update the file location references.', 'In the process of simplifying directory structure.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(5, 'Warning', 'pineapple-day-at-beach-5-200h.jpg', 'pineapple-day-at-beach-5-690w.jpg', 'include(): Failed opening ''data.php'' for inclusion (include_path=''.:/Applications/MAMP/bin/php/php7.0.12/lib/php'') in /Users/Sandman/Dropbox/web-content/rmit/blog/index.php on line 1', 'Tried to include a file that I had previously deleted as it was no longer needed. Neglected to update the page to remove the include statement.', 'In the process of simplifying directory structure.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(6, 'Fatal', 'pineapple-day-at-beach-6-200h.jpg', 'pineapple-day-at-beach-6-690w.jpg', 'Call to undefined function fetch() in C:UsersJeremDropboxweb-content\rmitlogarticles.php on line 40', 'This was essentially a syntax error. I had: $stmt-fetch() instead of: $stmt->fetch()', 'Trying to loop through all articles and output them to the page.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(7, 'Warning', 'pineapple-day-at-beach-7-200h.jpg', 'pineapple-day-at-beach-7-690w.jpg', 'Cannot modify header information - headers already sent by (output started at /Users/Sandman/Dropbox/web-content/rmit/blog/create.php:3) in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/php/insert.inc.php on line 23', 'I had a header redirect at the end of my insert script, but apparently I have already sent headers (output html), meaning the redirect failed. The database updated correctly, however, so there were no problems with insert script.', 'Trying to insert data into the database via the web form.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(8, 'Warning', 'pineapple-day-at-beach-8-200h.jpg', 'pineapple-day-at-beach-8-690w.jpg', 'Undefined variable: currentPage in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/html/head.inc.php on line 9', 'Needed to add the line $currentPage = basename($_SERVER[''SCRIPT_NAME'']);  to the article page. This variable determines which navbar element to highlight.', 'Going back to edit the article page.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(9, 'Notice', 'pineapple-day-at-beach-9-200h.jpg', 'pineapple-day-at-beach-9-690w.jpg', 'Undefined index: id in /Users/Sandman/Dropbox/web-content/rmit/blog/article.php on line 7', 'Same error as ''Undefined Variable'' but since $_GET is an array, then it becomes an undefined index ''id'' in the array. When the variable ''id'' is not found in the url, then the index is not instantiated.', 'Trying to set default value for article id if get variable not set.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(10, 'Parse', 'neon-1-200h.jpg', 'neon-1-690w.jpg', 'syntax error, unexpected ''if'' (T_IF) in /Users/Sandman/Dropbox/web-content/rmit/blog/article.php on line 11', 'Inadvertently forgot the line delimiter ;', 'Trying to handle errors when fetching article from the database.', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(11, 'Notice', 'neon-2-200h.jpg', 'neon-2-690w.jpg', 'Array to string conversion in /Users/Sandman/Dropbox/web-content/rmit/blog/includes/php/notifications.inc.php on line 8', '$stmt->errorInfo() returns an array, but I was trying to echo it out. Must use print_r() instead.', 'Trying to handle errors when fetching article from the database', 1, '2017-03-06 15:32:30', '2017-03-06 15:32:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Jeremy', 'Gates', 's3138222@student.rmit.edu.au', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(2, 'Timothy', 'Richardson', 's3622604@student.rmit.edu.au', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(3, 'Justin', 'Gauci', 's3397700@student.rmit.edu.au', '2017-03-06 15:32:30', '2017-03-06 15:32:30'),
(4, 'James', 'Driessen', 's3622626@student.rmit.edu.au', '2017-03-06 15:32:30', '2017-03-06 15:32:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
