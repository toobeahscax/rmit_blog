<?php 

session_start();

include('includes/php/insert.inc.php');

$stmt = $dbc->prepare('SELECT * FROM categories');
$stmt->execute();
$dbc = null;

$currentPage = basename($_SERVER['SCRIPT_NAME']); 

?>

<?php include('includes/html/head.inc.php'); ?>

	<body>
		
<!-- CONTAINER --------------------------->	
		<div class="container-fluid">
			
			<?php include('includes/html/header.inc.php'); ?>
			
			
<!---- MAIN ROW ------------------------------------------>
			<div class="main-row row">
				<div class="col-xs-12 col-sm-12 col-md-12">

					<div class="row">
						<div class="row-title main-row-title text-center col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
							<div><h3>Create</h3></div>
						</div>
					</div>
					
					<?php include('includes/php/notifications.inc.php'); ?>
					<?php if(isset($_SESSION['success'])) { unset($_SESSION['success']); } ?>
					<?php if(isset($_SESSION['error'])) { unset($_SESSION['error']); } ?>

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-4 col-lg-offset-4">
							
							<form name="create" method="POST" enctype="multipart/form-data" onsubmit="return validate()">
								
								<div class="form-group">
									<label for="category_id">Category</label>
									<select class="form-control" id="category_id" name="category_id">
									<?php while($row = $stmt->fetch()) { ?>
										<option value="<?php echo $row['id']; ?>"><?php echo $row['category']; ?></option>
									<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label for="image">Upload an Image</label><div class="warning"></div>
									<input type="file" id="image" name="image" /><div class="warning" id="warn-image"></div>
									<p class="help-block">Images must be smaller than 2MB in size.</p>
								</div>
								<div class="form-group">
									<label for="error_text">Error Text</label>
									<input type="text" class="form-control" id="error_text" name="error_text" value="<?php echo(isset($_POST['error_text']) ? $_POST['error_text'] : ''); ?>" />
									<div class="warning" id="warn-error_text"></div>
								</div>
								<div class="form-group">
									<label for="analysis">Analysis</label>
									<textarea type="text" class="form-control" id="analysis" name="analysis" rows=4><?php echo(isset($_POST['analysis']) ? $_POST['analysis'] : ''); ?></textarea>
									<div class="warning" id="warn-analysis"></div>
								</div>
								<div class="form-group">
									<label for="context">Context</label>
									<input type="text" class="form-control" id="context" name="context" value="<?php echo(isset($_POST['context']) ? $_POST['context'] : ''); ?>" />
									<div class="warning" id="warn-context"></div>
								</div>
								<div class="row">
									<button type="submit" class="btn btn-primary col-xs-5 col-xs-offset-1 col-sm-6 col-sm-offset-1 col-md-6 col-md-offset-1">Create</button>
									<a class="btn btn-danger col-xs-4 col-xs-offset-1 col-sm-3 col-sm-offset-1 col-md-3 col-md-offset-1" id="cancel_link" href="articles.php">Cancel</a>
								</div>
								
							</form>
							
						</div>
					</div>
					
				</div>
			</div>
<!---- MAIN ROW END -------------------------------------->
			
			
			<?php include('includes/html/footer.inc.php'); ?>
			
		</div>
<!-- CONTAINER END ----------------------->
		
		<?php include('includes/html/resources.inc.php'); ?>
		
		<script async src="js/validate_form.js"></script>
		
	</body>
</html>