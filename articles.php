<?php

session_start();

require_once 'includes/php/connection.inc.php';
//require_once 'includes/php/connection.azure.inc.php';

if(array_key_exists('search', $_POST)) { //IF SEARCH QUERY PERFORMED, RETRIEVE RELEVANT ARTICLES
	$searchTerm 				= $_POST['search'];
	//$sql = "SELECT a.id, a.image_thumb, c.category, a.error_text, a.context FROM articles a, categories c WHERE c.category LIKE '%$searchTerm%' OR a.error_text LIKE '%$searchTerm%' OR a.context LIKE '%$searchTerm%' AND a.category_id=c.id";
	$sql = "SELECT a.id, a.image_thumb, c.category, a.context FROM articles a, categories c WHERE c.category LIKE '%$searchTerm%' AND a.category_id=c.id";
	$stmt = $dbc->prepare($sql);
	$stmt->execute();
} else { //OTHERWISE, RETRIEVE ALL ARTICLES
	$sql = 'SELECT a.id, a.image_thumb, c.category, a.context FROM articles a, categories c WHERE a.category_id=c.id ORDER BY a.id';
	$stmt = $dbc->prepare($sql);
	$stmt->execute();
}
	 
$currentPage = basename($_SERVER['SCRIPT_NAME']); 

?>

<?php include('includes/html/head.inc.php'); ?>

	<body>
		
<!-- CONTAINER --------------------------->	
		<div class="container-fluid">

			<?php include('includes/html/header.inc.php'); ?>
			
			<?php include('includes/html/red-row.inc.php'); ?>

<!---- MAIN ROW ------------------------------------------>
			<div class="main-row row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">

					<div class="row">
						<div class="row-title main-row-title text-center col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
							<div><h3>Articles</h3></div>
						</div>
					</div>
					
					<?php include('includes/php/notifications.inc.php'); ?>
					<?php if(isset($_SESSION['success'])) { unset($_SESSION['success']); } ?>
					<?php if(isset($_SESSION['error'])) { unset($_SESSION['error']); } ?>
					
					<div class="row">
						
						<?php while($row = $stmt->fetch()) { ?>
						<div class="article-thumbnail col-xs-12 col-sm-6 col-md-4 col-lg-4">
							<div class="well">
								<div class="thumbnail"><img class="img-rounded" src="images/<?php echo htmlspecialchars($row['image_thumb']); ?>" /></div>
								<div class="text-center">
									<h3><?php echo htmlspecialchars($row['category']); ?> Error</h3>
									<p class="context"><?php echo htmlspecialchars($row['context']); ?></p>
										<button type="button" class="btn btn-default btn-block" onclick="window.location.href='article.php?id=<?php echo htmlspecialchars($row['id']); ?>'">
											Read More...
										</button>
								</div>
							</div>
						</div>
<!--						<pre>-->
							<?php //print_r($row); ?>
<!--						</pre>-->
						<?php } ?>
							
					</div>
					
					
				</div>
			</div>
<!---- MAIN ROW END -------------------------------------->


			<?php include('includes/html/footer.inc.php'); ?>

		</div>
<!-- CONTAINER END ----------------------->
		
		<?php include('includes/html/resources.inc.php'); ?>
		
	</body>
</html>